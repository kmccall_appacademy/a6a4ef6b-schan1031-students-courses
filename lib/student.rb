class Student
  def initialize(first,last)
    @first = first
    @last = last
    @courses = []
  end

  def first_name
    @first
  end

  def last_name
    @last
  end

  def courses
    @courses
  end

  def name
    @first + ' ' + @last
  end

  def enroll(course)
    @courses << course if !@courses.include?(course)
    @courses.last.students << self
  end

  def course_load
    cl = Hash.new{0}
    @courses.each do |c|
      cl[c.department] += c.credits
    end
    cl
  end
end
